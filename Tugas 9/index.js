class Animal{
  constructor(name){
    this.name = name
    this.legs = 4
    this.cold_blooded = false
  }
}

class Ape extends Animal {
  constructor(name){
    super(name);
    this.legs = 2
  }

  yell(){
    console.log("Auooo")
  }
}

class Frog extends Animal {
  constructor(name){
    super(name);
  }

  jump(){
    console.log("hop hop")
  }
}


let sheep = new Animal("Sapi")

let kera = new Ape("kera")

let kodok = new Frog("Kodok")
kodok.jump()

class Clock {
  constructor(template){
    this.template = template.template
  }
    timer;

  render =()=> {
  
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    let template = this.template;
    var output = template.replace('h', hours)
      .replace('m', mins)
      .replace('s', secs)

    console.log(output);
  }

  stop =()=> {
    clearInterval(this.timer);
  };

  start =()=> {
    this.render()
    this.timer = setInterval(this.render, 1000);
  };

}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 